package icom.utils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.util.IOUtils;
import icom.db.dynamo.DynamoDbCreds;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class CredUtils {

    private static DynamoDbCreds dynamoDbCreds;

    public static AWSCredentials awsCredentials;
    public static AwsClientBuilder.EndpointConfiguration amazonAWSEndpointConfig;

    static {
        initCredentialsFromFile();
        if (awsCredentials == null) {
            tryInitCredentialsFromAwsEnv();
        }
        initAmazonAWSCredentials();
        initAmazonAWSEndpointConfig();
    }

    private static void tryInitCredentialsFromAwsEnv() {
        dynamoDbCreds = new DynamoDbCreds(
                getVarFromEnvByName("aws_access_key_id"),
                getVarFromEnvByName("aws_secret_access_key"),
                getVarFromEnvByName("dynamoDb_endpoint"),
                getVarFromEnvByName("dynamoDb_region"));
    }

    private static String getVarFromEnvByName(String varName) {
        String var = System.getenv(varName);
        assert var != null;
        return var;

    }


    private static void initCredentialsFromFile() {
        String aws_home = System.getenv("aws_home");
        if (aws_home == null) {
            System.out.println("aws credentials path not found");
            return;
        }

        File credentialFile = new File(aws_home + "\\credentials");
        FileInputStream fileInputStream;
        String[] lines = new String[0];
        try {
            fileInputStream = new FileInputStream(credentialFile);
            lines = IOUtils.toString(fileInputStream).split(System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
        dynamoDbCreds = new DynamoDbCreds(getKeyByNameFromLines(lines, "aws_access_key_id"),
                getKeyByNameFromLines(lines, "aws_secret_access_key"),
                getKeyByNameFromLines(lines, "dynamoDb_endpoint"),
                getKeyByNameFromLines(lines, "dynamoDb_region"));
    }

    private static String getKeyByNameFromLines(String[] lines, String keyName) {
        return Arrays.stream(lines)
                .filter(s -> s.startsWith(keyName + "="))
                .flatMap(s -> Arrays.stream(s.split(keyName + "=")))
                .filter(s -> !s.isEmpty())
                .findFirst().orElseThrow(() -> new NullPointerException("value mustn't be null"));
    }

    public static void initAmazonAWSCredentials() {
        awsCredentials = new BasicAWSCredentials(
                dynamoDbCreds.getAccessKey(), dynamoDbCreds.getSecretKey());
    }

    public static void initAmazonAWSEndpointConfig() {
        amazonAWSEndpointConfig = new AwsClientBuilder
                .EndpointConfiguration(dynamoDbCreds.getAmazonDynamoDBEndpoint(), dynamoDbCreds.getAmazonDynamoDBRegion());
    }
}
