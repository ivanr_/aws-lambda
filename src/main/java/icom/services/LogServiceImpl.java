package icom.services;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.google.gson.Gson;
import icom.db.dynamo.Repository;
import icom.db.dynamo.RepositoryImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class LogServiceImpl implements LogService {

    public static final String DATE_PATTERN = "yyyy-MM-dd hh:mm:ss";
    private final Repository repository;
    private final Gson gson;
    private SimpleDateFormat simpleDateFormat;

    public LogServiceImpl() {
        this.repository = new RepositoryImpl();
        this.gson = new Gson();
    }

    @Override
    public String addLog(String logFromBody) {
        Map<String, String> logAsMap = gson.fromJson(logFromBody, Map.class);
        return repository.addLogToDb(logAsMap.get("text"), logAsMap.get("comment"), getDate(logAsMap.get("created")));
    }

    private String getDate(String created) {
        if (created == null) {
            return getFormatter().format(new Date());
        }
        return getFormatter().format(created);
    }

    private SimpleDateFormat getFormatter() {
        if (simpleDateFormat == null) {
            simpleDateFormat = new SimpleDateFormat(DATE_PATTERN);
        }
        return simpleDateFormat;
    }

    @Override
    public String get(String itemId) {
        List<Item> items;
        if (itemId == null) {
            items = repository.getAllLogs();
        } else {
            items = repository.getById(itemId);
        }
        return gson.toJson(items);
    }

    @Override
    public String removeById(String logId) {
        return repository.removeById(logId);
    }
}
