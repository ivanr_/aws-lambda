package icom.services;

public interface LogService {
    String addLog(String body);

    String get(String itemId);

    String removeById(String logId);
}
