package icom.db.dynamo;

import com.amazonaws.services.dynamodbv2.document.Item;

import java.util.List;

public interface Repository {
    String addLogToDb(String text, String comment, String created);

    List<Item> getAllLogs();

    String removeById(String logId);

    List<Item> getById(String itemId);
}
