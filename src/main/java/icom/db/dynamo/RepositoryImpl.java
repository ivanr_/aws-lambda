package icom.db.dynamo;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemUtils;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;

import java.util.*;

import static icom.utils.CredUtils.amazonAWSEndpointConfig;
import static icom.utils.CredUtils.awsCredentials;

public class RepositoryImpl implements Repository {

    public static final String LOG_TABLE_NAME = "logs";
    private final AmazonDynamoDB dynamoDbClient;

    public RepositoryImpl() {
        dynamoDbClient = AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(amazonAWSEndpointConfig)
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                                awsCredentials))
                .build();
    }


    @Override
    public List<Item> getAllLogs() {
        ScanResult logsResult = dynamoDbClient.scan(new ScanRequest().withTableName(LOG_TABLE_NAME));
        return ItemUtils.toItemList(logsResult.getItems());
    }

    @Override
    public String addLogToDb(String text, String comment, String created) {
        int logId = getMaxId() + 1;
        DynamoDB dynamoDb = new DynamoDB(dynamoDbClient);
        Table logTable = dynamoDb.getTable(LOG_TABLE_NAME);
        Item newItem = new Item()
                .withLong("id", logId)
                .withString("text", text)
                .withString("created", created);
        addComment(newItem, comment);
        logTable.putItem(newItem);
        return "item put successfully with id: " + logId;
    }

    private void addComment(Item newItem, String comment) {
        if (comment != null) {
            newItem.withString("comment", comment);
        }
    }

    private int getMaxId() {
        ScanResult logsResult = dynamoDbClient.scan(new ScanRequest().withTableName(LOG_TABLE_NAME));
        return logsResult.getItems().stream()
                .map(map -> map.get("id"))
                .map(attributeValue -> Integer.parseInt(attributeValue.getN()))
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IndexOutOfBoundsException("table didn't provide ids"));
    }

    @Override
    public String removeById(String logId) {
        Map<String, AttributeValue> item = new HashMap<>();
        item.put("id", new AttributeValue().withN(logId));
        dynamoDbClient.deleteItem(
                new DeleteItemRequest()
                        .withTableName(LOG_TABLE_NAME)
                        .withKey(item));
        return "deletion for item id " + logId + " successful";
    }

    @Override
    public List<Item> getById(String itemId) {
        return ItemUtils.toItemList(
                Collections.singletonList(
                        dynamoDbClient.getItem(
                                new GetItemRequest()
                                        .withTableName(LOG_TABLE_NAME)
                                        .withKey(Collections.singletonMap("id", new AttributeValue().withN(itemId)))
                                        .withConsistentRead(true)
                        ).getItem()));
    }
}
