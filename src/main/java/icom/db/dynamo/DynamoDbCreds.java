package icom.db.dynamo;

public class DynamoDbCreds {
    private final String accessKey;
    private final String secretKey;
    private final String amazonDynamoDBEndpoint;
    private final String amazonDynamoDBRegion;

    public DynamoDbCreds(String accessKey, String secretKey, String amazonDynamoDBEndpoint, String amazonDynamoDBRegion) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.amazonDynamoDBEndpoint = amazonDynamoDBEndpoint;
        this.amazonDynamoDBRegion = amazonDynamoDBRegion;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getAmazonDynamoDBEndpoint() {
        return amazonDynamoDBEndpoint;
    }

    public String getAmazonDynamoDBRegion() {
        return amazonDynamoDBRegion;
    }
}
