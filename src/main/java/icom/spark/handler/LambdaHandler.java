package icom.spark.handler;

import com.amazonaws.serverless.exceptions.ContainerInitializationException;
import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.serverless.proxy.spark.SparkLambdaContainerHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import icom.services.LogService;
import icom.services.LogServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

import java.util.Date;

import static spark.Spark.*;

public class LambdaHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {

    public static final String ITEM_ID_PARAM_NAME = "itemId";
    private boolean initialized = false;
    private final SparkLambdaContainerHandler<AwsProxyRequest, AwsProxyResponse> handler = SparkLambdaContainerHandler.getAwsProxyHandler();
    private final Logger log = LoggerFactory.getLogger(LambdaHandler.class);

    private final LogService logService;

    public LambdaHandler() throws ContainerInitializationException {
        logService = new LogServiceImpl();
    }

    @Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyRequest, Context context) {
        if (!initialized) {
            log.warn("initialized at " + new Date());
            defineRoutes();
            initialized = true;
        }
        return handler.proxy(awsProxyRequest, context);
    }

    private void defineRoutes() {
        get("/welcome", (request, response) -> "Welcome from get request");
        get("/log/get", this::assignGet);
        get("/log/get/:" + ITEM_ID_PARAM_NAME, this::assignGet);
        get("/*", this::assignGetNotImplemented);

        post("/log/add", (request, response) -> assignPost(request));

        get("/log/delete", this::getInfoMessageForDeleteFromGetMethod);
        get("/log/delete/:" + ITEM_ID_PARAM_NAME, this::getInfoMessageForDeleteFromGetMethod);
        delete("/log/delete/:itemId", (request, response) -> assignDelete(request));
    }

    private String assignGetNotImplemented(Request request, Response response) {
        return "GET is not implemented, use these: /welcome, /log/get, /log/get/{itemId}";
    }

    private String assignDelete(Request request) {
        return logService.removeById(request.params(ITEM_ID_PARAM_NAME));
    }

    private String getInfoMessageForDeleteFromGetMethod(Request request, Response response) {
        String itemId = request.params(ITEM_ID_PARAM_NAME);
        return "please use delete rest method to get remove record" +
                (itemId == null ? "" : " with id " + itemId);
    }

    private String assignPost(Request request) {
        return logService.addLog(request.body());
    }

    private String assignGet(Request request, Response response) {
        return logService.get(request.params("itemId"));
    }
}
