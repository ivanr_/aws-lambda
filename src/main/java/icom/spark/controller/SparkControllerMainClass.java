package icom.spark.controller;

import spark.Request;
import spark.Response;

import static spark.Spark.*;

public class SparkControllerMainClass {
    public static void main(String[] args) {
        get("/hello/:name", SparkControllerMainClass::getHelloWorld);
        post("/post", SparkControllerMainClass::getResponse);
        delete("/delete/:id", SparkControllerMainClass::getDeleteMethod);
        get("/*", (request, response) -> "not implemented");
    }

    private static String getHelloWorld(Request request, Response response) {
        return "Hello World, requested params: " + request.params("name");
    }

    private static String getResponse(Request request, Response response) {
        System.out.println("post method requested");
        System.out.println("request body: " + request.body());
        return "response from post method";
    }

    private static String getDeleteMethod(Request request, Response response){
        System.out.println("delete method requested");
        return "delete done for id: " + request.params("id");
    }
}
